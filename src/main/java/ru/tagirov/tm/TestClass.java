package ru.tagirov.tm;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.init.ServiceLocator;
import ru.tagirov.tm.util.DateUtil;
import ru.tagirov.tm.util.Md5Util;
import ru.tagirov.tm.util.UUIDUtil;

public class TestClass {
    public ServiceLocator serviceLocator;

    public TestClass(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


    public void test(){
        serviceLocator.getIUserService().persist(new User(UUIDUtil.getUuid(), "admin", "admin", Md5Util.getMd5("0000"), Role.getRole("admin"), DateUtil.getDate()));
        serviceLocator.getIUserService().persist(new User(UUIDUtil.getUuid(), "Zakir", "zakir", Md5Util.getMd5("12345"), Role.getRole("user"), DateUtil.getDate()));
        serviceLocator.getIUserService().persist(new User(UUIDUtil.getUuid(), "Timur", "timur", Md5Util.getMd5("23456"), Role.getRole("user"), DateUtil.getDate()));
        serviceLocator.getIUserService().persist(new User(UUIDUtil.getUuid(), "Azat", "azat", Md5Util.getMd5("34567"), Role.getRole("user"), DateUtil.getDate()));
        serviceLocator.getIUserService().persist(new User(UUIDUtil.getUuid(), "Aidar", "aidar", Md5Util.getMd5("45678"), Role.getRole("user"), DateUtil.getDate()));
        serviceLocator.getIUserService().persist(new User(UUIDUtil.getUuid(), "Airat", "airat", Md5Util.getMd5("56789"), Role.getRole("user"), DateUtil.getDate()));

        for (User tmp : serviceLocator.getIUserService().findAll()){
            if(!(tmp.getRole().getTitle().equalsIgnoreCase("admin"))) {
                serviceLocator.getIProjectService().persist(new Project(UUIDUtil.getUuid(), "This project is " + tmp.getName() + "." + " Project №1", "description1", DateUtil.getDate(), tmp.getId()));
                serviceLocator.getIProjectService().persist(new Project(UUIDUtil.getUuid(), "This project is " + tmp.getName() + "." + " Project №2", "description2", DateUtil.getDate(), tmp.getId()));
                serviceLocator.getIProjectService().persist(new Project(UUIDUtil.getUuid(), "This project is " + tmp.getName() + "." + " Project №3", "description3", DateUtil.getDate(), tmp.getId()));
                serviceLocator.getITaskService().persist(new Task(UUIDUtil.getUuid(), "This task is " + tmp.getName() + "." + " Task №1", "description1", DateUtil.getDate(), tmp.getId()));
                serviceLocator.getITaskService().persist(new Task(UUIDUtil.getUuid(), "This task is " + tmp.getName() + "." + " Task №2", "description1", DateUtil.getDate(), tmp.getId()));
            }

        }
    }
}