package ru.tagirov.tm;

import ru.tagirov.tm.command.adminCommand.*;
import ru.tagirov.tm.command.allCommand.*;
import ru.tagirov.tm.command.projectCommand.*;
import ru.tagirov.tm.command.taskCommand.*;
import ru.tagirov.tm.command.userCommand.*;
import ru.tagirov.tm.init.Bootstrap;

import java.io.IOException;

public class Application {

    private static final Class[] CLASSES = {
            AdminRemoveToUserCommand.class, AdminShowAllUsersCommand.class,
            AdminShowProjectsToUserCommand.class, AdminShowTaskToProjectToUserCommand.class,
            AdminShowTaskToUserCommand.class, AdminShowUserCommand.class,

            AboutCommand.class, ExitCommand.class, HelpCommand.class,
            LoginCommand.class, RegistrationCommand.class,

            ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectListCommand.class, ProjectRemoveCommand.class, ProjectUpdateCommand.class,

            TaskAddToProjectCommand.class, TaskClearCommand.class, TaskClearToProjectCommand.class,
            TaskCreateCommand.class, TaskCreateToProjectCommand.class, TaskListCommand.class,
            TaskListToProjectCommand.class, TaskRemoveCommand.class, TaskRemoveToProjectCommand.class,
            TaskUpdateCommand.class, TaskUpdateToProjectCommand.class,

            UserClearAllCommand.class, UserListAllCommand.class,
            UserPasswordUpdateCommand.class, UserShowProfileCommand.class,
            UserUpdateProfileCommand.class
    };
    public static void main( String[] args ) throws IOException, IllegalAccessException, InstantiationException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
        bootstrap.start();
    }
}
