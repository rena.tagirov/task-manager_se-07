package ru.tagirov.tm.init;

import ru.tagirov.tm.TestClass;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.repository.IProjectRepository;
import ru.tagirov.tm.repository.ITaskRepository;
import ru.tagirov.tm.repository.IUserRepository;
import ru.tagirov.tm.repository.impl.ProjectRepository;
import ru.tagirov.tm.repository.impl.TaskRepository;
import ru.tagirov.tm.repository.impl.UserRepository;
import ru.tagirov.tm.service.IProjectService;
import ru.tagirov.tm.service.ITaskService;
import ru.tagirov.tm.service.IUserService;
import ru.tagirov.tm.service.impl.ProjectService;
import ru.tagirov.tm.service.impl.TaskService;
import ru.tagirov.tm.service.impl.UserService;
import ru.tagirov.tm.util.TerminalService;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bootstrap implements ServiceLocator{

    public Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final IProjectRepository<Project> iProjectRepository = new ProjectRepository();
    private final ITaskRepository<Task> iTaskRepository = new TaskRepository();
    private final IUserRepository<User> iUserRepository = new UserRepository();
    private final IProjectService<Project> projectService = new ProjectService(iProjectRepository);
    private final ITaskService<Task> taskService = new TaskService(iTaskRepository);
    private final IUserService<User> userService = new UserService(iUserRepository);

    private User user;
    private TestClass testClass = new TestClass(this);
    String str;


    public void start() throws IOException{
        testClass.test();
        System.out.println("***WELCOME TO TASK MANAGER***");
        while (!(str = TerminalService.service()).equalsIgnoreCase("EXIT TO TASK MANAGER")) {
            if (user == null) {
                for (AbstractCommand tmp : getCommands()) {
                    if (tmp.getName().equalsIgnoreCase(str) && tmp.getRoleCommand().equals("all")) {
                        tmp.execute();
                    }
                }
            } else if (user.getRole().getTitle().equals("user")) {
                for (AbstractCommand tmp : getCommands()) {
                    if (tmp.getName().equalsIgnoreCase(str) && (tmp.getRoleCommand().equals("user") || tmp.getRoleCommand().equals("all"))) {
                        tmp.execute();
                    } else if (tmp.getName().equalsIgnoreCase(str) && tmp.getRoleCommand().equals("admin")) {
                        System.out.println("[THE COMMAND IS NOT VALID!]");
                        System.out.println();
                    }
                }
            } else if (user.getRole().getTitle().equals("admin")) {
                for (AbstractCommand tmp : getCommands()) {
                    if (tmp.getName().equalsIgnoreCase(str)) {
                        tmp.execute();
                    }
                }
            } else {
                System.out.println("[THE COMMAND IS NOT VALID!]");
                System.out.println("[YOU ARE LOGGER OUT OF YOUR ACCOUNT!]");
                System.out.println();
            }
        }
    }

    private void registry(final AbstractCommand command) {
        final String commandName = command.getName();
        final String commandDescription = command.getDescription();

        if (commandName == null || commandName.isEmpty()) {
            System.out.println("Command don't registration!");
        }
        if (commandDescription == null || commandDescription.isEmpty()) {
            System.out.println("Command don't registration!");
        }
        command.setServiceLocator(this);
        commands.put(commandName, command);
    }

    public void init(Class<? extends AbstractCommand>[] CLASSES) throws IllegalAccessException, InstantiationException {
        for (final Class<? extends AbstractCommand> abstractClass : CLASSES) {
            registry(abstractClass.newInstance());
        }
    }

    @Override
    public IProjectService<Project> getIProjectService() {
        return projectService;
    }

    @Override
    public ITaskService<Task> getITaskService() {
        return taskService;
    }

    @Override
    public IUserService<User> getIUserService() {
        return userService;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }
}