package ru.tagirov.tm.init;

import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.service.IProjectService;
import ru.tagirov.tm.service.ITaskService;
import ru.tagirov.tm.service.IUserService;

import javax.jws.soap.SOAPBinding;
import java.util.Collection;
import java.util.List;

public interface ServiceLocator {

    IProjectService<Project> getIProjectService();

    ITaskService<Task> getITaskService();

    IUserService<User> getIUserService();

    void setUser(User user);

    User getUser();

    Collection<AbstractCommand> getCommands();

}
