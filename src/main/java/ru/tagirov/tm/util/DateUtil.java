package ru.tagirov.tm.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static SimpleDateFormat formatForDateNow  = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

    public static String getDate(){
        Date date = new Date();
        return formatForDateNow.format(date);
    }
}